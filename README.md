# bs-service-worker

This package is the home of BuckleScript bindings for the
[JavaScript service worker API](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API). 
It is currently in a fairly early stage of development.

Right now, I'm just cranking it out as fast as I can, without a lot of concern
about things like good design or testing. Do *not* be surprised if you find
plenty of mistakes. 

On the plus side, this is very much an actively maintained project. If you see
something, say something, and I'll try to get a fix turned around within a day
or two. 

## on the ReScript Rebrand

I'm ignoring it until things have settled down just a little. After that, I'm
thinking I'll fork the project and maintain two versions, just in case some
people aren't as willing as I am to upgrade. 

## Installation
`npm install bs-service-worker`

## Usage
This is meant to be a tight binding around the JavaScript API, and is intended
to feel idiomatic to JavaScript. I don't have an example written yet, but 
I fully intend to return to this once I've written the app I want to write 
(which was the reason I'm writing this binding to begin with).

## Implemented
- [X] [AbstractWorker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- [x] [Cache](https://developer.mozilla.org/en-US/docs/Web/API/Cache)
- [x] [CacheMatchOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/match)
- [x] [CacheDeleteOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/delete)
- [x] [CacheKeysOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/keys)
- [x] [CacheStorage](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorageJ)
- [x] [CacheStorageOptions](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorage/match)
- [x] [Client](https://developer.mozilla.org/en-US/docs/Web/API/Client)
- [x] [ClientsMatchAllOptions](https://developer.mozilla.org/en-US/docs/Web/API/Clients/matchAll)
- [x] [Clients](https://developer.mozilla.org/en-US/docs/Web/API/Clients)
- [X] [ContainerRegisterOptions](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register)
- [x] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent) 
- [x] [ExtendableMessageEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableMessageEvent)
- [x] [ExtendableMessageEventOptions](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableMessageEvent/ExtendableMessageEvent)
- [x] [ExtendableMessageEventSource](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableMessageEvent/ExtendableMessageEvent)
- [x] [FetchEvent](https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent)
- [X] [InstallEvent](https://developer.mozilla.org/en-US/docs/Web/API/InstallEvent)
- [X] [MessageEvent](https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent)
- [X] [MessagePort](https://developer.mozilla.org/en-US/docs/Web/API/MessagePort)
- [ ] NavigationPreloadManager
- [ ] NavigationPreload 
- [X] Navigator.serviceWorker (under a type called ServiceWorkerNavigator).
- [X] [PromiseRejectionEvent](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent)
- [X] [PromiseRejectionEventOptions](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent/PromiseRejectionEvent)
- [X] [PromiseRejectionEventType](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent/PromiseRejectionEvent)
- [X] [ServiceWorker](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker)
- [X] [ServiceWorkerContainer](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer)
- [X] [ServiceWorkerGlobalScope](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope)
- [X] [ServiceWorkerNotificationOptions](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/getNotifications)
- [X] [ServiceWorkerRegistration](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration)
- [X] [ServiceWorkerState](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerState)
- [ ] SyncEvent
- [ ] SyncManager
- [X] [Worker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- [X] [WorkerCredentials](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [X] [WorkerOptions](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [X] [WorkerType](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [x] [WindowClient](https://developer.mozilla.org/en-US/docs/Web/API/WindowClient)

## Notes
### on EventTargets
I'm hoping to crack the code for how to perfectly enforce types onto 
`addEventListener`... someday. I haven't done it yet. For now, stick to the 
function-valued properties. Those are typed pretty well.

### on NavigationPreloadManager and NavigationPreloadState
My current thinking is to omit NavigationPreloadManager and 
NavigationPreloadState because they're too dependent on ByteString, and I 
don't want to expand the scope of my project. If you badly need these 
types or if 
you know of a ByteString implementation I could depend on, feel free to 
open an
issue.

### on ServiceWorkerGlobalScope
It's not fully implemented because it implements a ton of other interfaces.
Fully implementing it is a nice-to-have. If there's something you need from
this type that I haven't implemented, feel free to open an issue--it'll help
me prioritize. 

Also, currently the `navigator` property lives in 'ServiceWorkerGlobalScope',
even though technically it's a part of WorkerGlobalScope.

### on TODOs
I put in a TODO everyplace where there's a (non-deprecated) method or type
I didn't implement, whether or not I had any intention of implementing it. If
you go looking for something in here and find a TODO in its place, feel free
to open an issue, submit a pull request, or just @ me on 
[Twitter](https://twitter.com/webbureaucrat) and we can talk about how to get
you what you need. 

### Nullable Properties
I'm realizing I've definitely been playing it fast and loose with getters on
properties returning a type instead of an option of a type. I intend to fix 
that, but that means that for the time being there will be some inconsistentcy
within the codebase as to how these are handled. 

## For further reading
I *strongly* recommend you check out my 
[catch-all documentation](https://webbureaucrat.gitlab.io/posts/issues-and-contribution/)
on my 
projects. It describes how to get in touch with me if you have any questions, 
how to contribute code, coordinated disclosure of security vulnerabilities, 
and more. It will be regularly updated with any information I deem relevant
to my side projects. 

Most of all, feel free to get in touch! I'm delighted to hear suggestions
or field questions. 
