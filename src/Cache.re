open Fetch;

type cache;

type req = Request(Request.t) | String(string);

module Private = {
  module Delete = {
    [@bs.send]
    external withOptions:
      (cache, req, CacheDeleteOptions.t) => Js.Promise.t(bool) =
      "delete";

    [@bs.send]
    external withoutOptions: (cache, req) => Js.Promise.t(bool) =
      "delete";
  };

  module Keys = {
    module WithRequest = {
      [@bs.send]
      external withoutOptions:
        (cache, req) => Js.Promise.t(array(Request.t)) =
        "keys";

      [@bs.send]
      external withOptions:
        (cache, req, CacheMatchOptions.t) =>
        Js.Promise.t(array(Request.t)) =
        "keys";
    };

    module WithoutRequest = {
      [@bs.send]
      external withoutOptions: cache => Js.Promise.t(array(Request.t)) =
        "keys";

      [@bs.send]
      external withOptions:
        (cache, CacheMatchOptions.t) => Js.Promise.t(array(Request.t)) =
        "keys";
    };
  };
  module Match = {
    [@bs.send]
    external withoutOptions: (cache, Request.t)
      => Js.Promise.t(Js.Nullable.t(Response.t))
      = "match";

    [@bs.send]
    external withOptions: (cache, Request.t, CacheMatchOptions.t)
      => Js.Promise.t(Js.Nullable.t(Response.t))
      = "match";
  };

  module MatchAll = {
    module WithRequest = {
      [@bs.send]
      external withoutOptions:
        (cache, Request.t) => Js.Promise.t(array(Response.t)) =
        "match";

      [@bs.send]
      external withOptions:
        (cache, Request.t, CacheMatchOptions.t) =>
        Js.Promise.t(array(Response.t)) =
        "match";
    };

    module WithoutRequest = {
      [@bs.send]
      external withoutOptions: cache => Js.Promise.t(array(Response.t)) =
        "match";

      [@bs.send]
      external withOptions:
        (cache, CacheMatchOptions.t) => Js.Promise.t(array(Response.t)) =
        "match";
    };
  };
};

let match = (cache, ~options=?, ~req: Request.t):
  Js.Promise.t(Js.Nullable.t(Response.t)) => {
  switch (options) {
  | None => Private.Match.withoutOptions(cache, req)
  | Some(o) => Private.Match.withOptions(cache, req, o)
  };
};

let matchAll = (~options=?, ~req=?, cache): Js.Promise.t(array(Response.t))
  => {
  switch (req) {
  | None =>
    switch (options) {
    | None => Private.MatchAll.WithoutRequest.withoutOptions(cache)
    | Some(o) => Private.MatchAll.WithoutRequest.withOptions(cache, o)
    }
  | Some(r) =>
    switch (options) {
    | None => Private.MatchAll.WithRequest.withoutOptions(cache, r)
    | Some(o) => Private.MatchAll.WithRequest.withOptions(cache, r, o)
    }
  };
};

[@bs.send] external add: (cache, req) => Js.Promise.t(unit) = "add";

[@bs.send]
external addAll: (cache, array(req)) => Js.Promise.t(unit) = "addAll";

[@bs.send]
external put: (cache, Request.t, Response.t) => Js.Promise.t(unit) = "put";

let delete = (cache, ~options=?, ~req: req): Js.Promise.t(bool) => {
  switch (options) {
  | None => Private.Delete.withoutOptions(cache, req)
  | Some(o) => Private.Delete.withOptions(cache, req, o)
  };
};

let keys = (~options=?, ~req=?, cache): Js.Promise.t(array(Request.t)) => {
  switch (req) {
  | None =>
    switch (options) {
    | None => Private.Keys.WithoutRequest.withoutOptions(cache)
    | Some(o) => Private.Keys.WithoutRequest.withOptions(cache, o)
    }
  | Some(r) =>
    switch (options) {
    | None => Private.Keys.WithRequest.withoutOptions(cache, r)
    | Some(o) => Private.Keys.WithRequest.withOptions(cache, r, o)
    }
  };
};

type t = cache;
