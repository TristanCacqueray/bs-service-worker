open Fetch;

type cacheStorage;

module Private = {
  module Match = {
    [@bs.send]
    external withOptions:
      (cacheStorage, Request.t, CacheStorageOptions.t)
      => Js.Promise.t(Js.Nullable.t(Response.t))
      = "delete";

    [@bs.send]
    external withoutOptions: (cacheStorage, Request.t)
      => Js.Promise.t(Js.Nullable.t(Response.t))
      = "delete";
  }
}     

let match = (cacheStorage, ~options=?, ~req: Request.t):
  Js.Promise.t(Js.Nullable.t(Response.t)) => {
  switch (options) {
  | None => Private.Match.withoutOptions(cacheStorage, req)
  | Some(o) => Private.Match.withOptions(cacheStorage, req, o)
  };
};

[@bs.send]
external has: (cacheStorage, string) => Js.Promise.t(bool)
  = "has";

[@bs.send]
external open_: (cacheStorage, string)
  => Js.Promise.t(Cache.t)
  = "open";

[@bs.send]
external delete: (cacheStorage, string)
  => Js.Promise.t(bool)
  = "delete";

[@bs.send]
external keys: (cacheStorage) => Js.Promise.t(array(string)) = "keys";

type t = cacheStorage;
