type _t('a);
type t_like('a) = Notifications.ExtendableEvent.t_like(_t('a));
type t = t_like(Dom._baseClass);

module Private
{
  module Make
  {
    [@bs.new] external withoutInit: string => t = "ExtendableMessageEvent";
    [@bs.new]
    external withInit: (string, ExtendableMessageEventOptions.t('data)) => t
      = "ExtendableMessageEvent";
  };
};

/* TODO okay I'm pretty sure `type_` should be some kind of polymorphic
 * variant / union type (w/e) but it's not listed on MDN right now, and I
 * highly doubt anyone is going to use this constructor.

 * Usual rules: If you're using this and you want me to do better, open an
 * issue, and I'll give you nicer types. */

let make = (~init=?, ~type_: string): t =>
{
  switch (init) {
  | None => Private.Make.withoutInit(type_)
  | Some(i) => Private.Make.withInit(type_, i)
  };
};

/* Properties */

[@bs.get] external data: t_like('subtype) => 'data = "data";

[@bs.get] external origin: t_like('subtype) => Client.t = "origin";

/* MDN says this is an empty string. I'm a little confused. */
[@bs.get] external lastEventId: t_like('subtype) => string = "lastEventId";

/* MDN says this has to be a Client. I'm more than skeptical. In the
 * `init` type in the constructor it can be any `ExtendableMessageEventSource`.
 */
[@bs.get] external source: t_like('subtype) => ExtendableMessageEventSource.t
  = "source";

[@bs.get] external ports: t_like('subtype) => array(MessagePort.t) = "ports";