open Fetch;

type _t('a);
type t_like('a) = Notifications.ExtendableEvent.t_like(_t('a));
type t = t_like(Dom._baseClass);

/* mdn calls this constructor "rarely used." */
[@bs.new] external make : t = "FetchEvent";

/* properties */
[@bs.get] external clientId: t => string  = "clientId";
[@bs.get] external preloadResponse: t => Js.Promise.t(Response.t) =
  "preloadResponse";
[@bs.get] external replacesClientId: t => string = "replacesClientId";
[@bs.get] external resultingClientId: t => string =
  "resultingClientId";
[@bs.get] external request: t => Request.t = "request";

/* methods */
[@bs.send] external respondWith: (t, Response.t) => unit
  = "respondWith";

// also inherits waitUntil from ExtendableEvent.
