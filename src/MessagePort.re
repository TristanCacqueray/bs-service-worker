type _t('a);
type t_like('a) = Dom.eventTarget_like(_t('a));
type t = t_like(Dom._baseClass);

module Private
{
  module PostMessage
  {
    [@bs.send]
    external withTransferList: (t, 'message, 'transferList)
      => unit
      = "postMessage";
    [@bs.send]
    external withoutTransferList: (t, 'message)
      => unit
      = "postMessage";
  };  
};

/* methods */

let postMessage = (t, ~transferList=?, ~message: 'message): unit =>
{
  switch (transferList) {
  | None => Private.PostMessage.withoutTransferList(t, message);
  | Some(o) => Private.PostMessage.withTransferList(t, message, o);
  };
};

[@bs.send] external start: t => unit = "start";

[@bs.send] external close: t => unit = "close";

/* TODO Event Handlers
 * I'm not quite sure how to break the dependency cycle here. */
