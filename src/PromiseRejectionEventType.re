[@bs.deriving jsConverter]
type t = [
  | `rejectionhandled
  | `unhandledrejection
];