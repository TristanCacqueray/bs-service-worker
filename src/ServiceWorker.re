/* Theoretically, ServiceWorker inherits from the Worker type, but not
 * in practice. In practice, according to MDN, Worker has one method that
 * ServiceWorker does not have (specifically, `terminate`), so I've copied over
 * all these properties and methods from Worker.
 */

type _t('a);
type t_like('a) = AbstractWorker.t_like(_t('a));
type t = t_like(Dom._baseClass);


module Private
{

  module PostMessage
  {
    [@bs.send]
    external withTransferList: (t, 'message, 'transferList)
      => unit
      = "postMessage";
    [@bs.send]
    external withoutTransferList: (t, 'message)
      => unit
      = "postMessage";
  }; 
};


/* Properties */

type messageHandler = MessageEvent.t => unit;

[@bs.get]
external get_onmessage: t => Js.Nullable.t(messageHandler) = "onmessage";

[@bs.set]
external set_onmessage: (t, messageHandler) => unit = "onmessage";

/* TODO is there a name for this type already? */
type errorHandler = unit => unit;

[@bs.get]
external get_onmessageerror: t => Js.Nullable.t(errorHandler)
  = "onmessageerror";

[@bs.set]
external set_onmessageerror: (t, errorHandler) => unit = "onmessageerror";

[@bs.get]
external
get_onrejectionhandled: PromiseRejectionEvent.t('a, 'reason)
  => PromiseRejectionEvent.handler('a, 'reason)
  = "onrejectionhandled";

[@bs.set]
external
set_onrejectionhandled:
  (PromiseRejectionEvent.t('a, 'reason),
  PromiseRejectionEvent.handler('a, 'reason))
  => unit
  = "onrejectionhandled";

[@bs.get]
external
get_onunhandledrejection: PromiseRejectionEvent.t('a, 'reason)
  => PromiseRejectionEvent.handler('a, 'reason)
  = "onunhandledrejection";

[@bs.set]
external
  set_onunhandledrejection:
    (PromiseRejectionEvent.t('a, 'reason),
    PromiseRejectionEvent.handler('a, 'reason))
    => unit
    = "onunhandledrejection";

[@bs.get] external scriptUrl: t => string = "scriptUrl";

[@bs.get] external state: t => ServiceWorkerState.t = "state";

/* Methods */

let postMessage = (t, ~transferList=?, ~message: 'message): unit =>
{
  switch (transferList) {
  | None => Private.PostMessage.withoutTransferList(t, message);
  | Some(o) => Private.PostMessage.withTransferList(t, message, o);
  };
};
