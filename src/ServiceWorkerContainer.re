type t;

/* Properties */

[@bs.get]
external controller: t => Js.Nullable.t(ServiceWorker.t) = "controller";

[@bs.get] external ready: t => Js.Promise.t(unit) = "ready";

/* Events */

type handler = Dom.event => unit;

[@bs.get]
external get_oncontrollerchange: t => Js.Nullable.t(handler)
  = "oncontrollerchange";

[@bs.set]
external set_oncontrollerchange: (t, handler) => unit = "oncontrollerchange";

type errorHandler = Dom.errorEvent => unit;

[@bs.get] external get_onerror: t => Js.Nullable.t(errorHandler) = "onerror";

[@bs.set] external set_onerror: (t, errorHandler) => unit = "onerror";

type messageHandler = MessageEvent.t => unit;

[@bs.get]
external get_onmessage: t => Js.Nullable.t(messageHandler) = "onmessage";

[@bs.set] external set_onmessage: (t, messageHandler) => unit = "onmessage";

/* Methods */

module Private
{
  module Register
  {
    [@bs.send]
    external withOptions: (t, string, ContainerRegisterOptions.t)
      => Js.Promise.t(ServiceWorkerRegistration.t)
      = "register";

    [@bs.send]
    external withoutOptions: (t, string)
      => Js.Promise.t(ServiceWorkerRegistration.t)
      = "register";
  };

  module GetRegistration
  {
    [@bs.send] external withoutScope: t
      => Js.Promise.t(Js.Nullable.t(ServiceWorkerRegistration.t))
      = "register";

    [@bs.send] external withScope: (t, string)
      => Js.Promise.t(Js.Nullable.t(ServiceWorkerRegistration.t))
      = "register";
  };


};

let register = (t, ~options=?, string):
  Js.Promise.t(ServiceWorkerRegistration.t) =>
  {
    switch(options) {
    | None => Private.Register.withoutOptions(t, string);
    | Some(o) => Private.Register.withOptions(t, string, o);
    };
  };

let getRegistration = (~scope=?, t):
  Js.Promise.t(Js.Nullable.t(ServiceWorkerRegistration.t)) =>
  {
    switch(scope) {
    | None => Private.GetRegistration.withoutScope(t);
    | Some(s) => Private.GetRegistration.withScope(t, s);
    };
  };

[@bs.send] external getRegistrations: t
  => Js.Promise.t(array(ServiceWorkerRegistration.t))
  = "getRegistrations";

[@bs.send] external startMessages: t => unit = "startMessages";
