/* TODO Okay, folks. This type inherits a ton of interfaces. I'd implement the
 * whole browser ecosystem before I'd finish this type if I really did it
 * right.

 * It looks like for most use cases, users of this type will mainly just need
 * the methods and properties that are specific to this type, so for the
 * moment, I'm going to operate on that assumption and just inherit
 * `Dom.eventTarget`. That said, if you need more, open an issue, and I'll
 * work on wht you need.
 */

type _t('a);
type t_like('a) = Dom.eventTarget_like(_t('a));
type t = t_like(Dom._baseClass);

[@bs.val] external self: t = "self";

/* Properties */

[@bs.get] external clients: t => array(Client.t) = "clients";

[@bs.get]
external registration: t => ServiceWorkerRegistration.t = "registration";

[@bs.get] external caches: t => CacheStorage.t = "caches";

/* inherited from WorkerGlobalScope */
[@bs.get] external navigator: t => ServiceWorkerNavigator.t = "navigator";


/* Events */
type extendableEventHandler('subtype) =
     Notifications.ExtendableEvent.t_like('subtype) => unit;

[@bs.get]
external get_onactivate: t => Js.Nullable.t(extendableEventHandler('subtype))
  = "onactivate";

[@bs.set]
external set_onactivate:
         (t, extendableEventHandler('subtype)) => unit = "onactivate";

type handler = Dom.event => unit;

[@bs.get]
external get_oncontentdelete: t => Js.Nullable.t(handler) = "oncontentdelete";

[@bs.set]
external set_oncontentdelete: (t, handler) => unit = "oncontentdelete";

type fetchHandler = FetchEvent.t => unit;

[@bs.get] external get_onfetch: t => Js.Nullable.t(fetchHandler) = "onfetch";

[@bs.set] external set_onfetch: (t, fetchHandler) => unit = "onfetch";

type installHandler = InstallEvent.t => unit;

[@bs.get]
external get_oninstall: t => Js.Nullable.t(installHandler) = "oninstall";

[@bs.set] external set_oninstall: (t, installHandler) => unit = "oninstall";

type extendableMessageHandler = ExtendableMessageEvent.t => unit;

[@bs.get]
external get_onmessage: t => Js.Nullable.t(extendableMessageHandler)
  = "onmessage";

[@bs.set] external set_onmessage: (t, extendableMessageHandler) => unit
 = "onmessage";

type notificationHandler('data)
  = Notifications.NotificationEvent.t('data) => unit;

[@bs.get] external get_onnotificationclick: t
  => Js.Nullable.t(notificationHandler('data))
  = "notificationclick";

[@bs.set] external set_onnotificationclick: (t, notificationHandler('data))
  => unit
  = "onnotificationclick";

[@bs.get]
external get_onnotificationclose: t
  => Js.Nullable.t(notificationHandler('data))
  = "notificationclose";

[@bs.set]
external set_onnotificationclose: (t, notificationHandler('data)) => unit
  = "notificationclose";

type pushHandler('data) = Push.PushEvent.t('data) => unit;

[@bs.get]
external get_onpush: t => Js.Nullable.t(pushHandler('data)) = "onpush";

[@bs.set] external set_onpush: (t, pushHandler('data)) => unit = "onpush";

/* TODO onpushsubscriptionchange  requires PushSubscriptionChangeHandler,
 * which isn't in MDN
 * yet.*/

/* TODO onsync requires SyncEvent */

/* Methods */
[@bs.send] external skipWaiting: t => Js.Promise.t(unit) = "skipWaiting";

// TODO fetch should just use bs-fetch, right?
