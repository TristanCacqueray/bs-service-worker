type _t('a);
type t_like('a) = Dom.eventTarget_like(_t('a));
type t = t_like(Dom._baseClass);

module Private
{
  module GetNotifications
  {
    [@bs.send]
    external withOptions: (t, ServiceWorkerNotificationOptions.t)
      => Js.Promise.t(array(Notifications.Notification.t('data)))
      = "getNotifications";
    [@bs.send] external withoutOptions: t
      => Js.Promise.t(array(Notifications.Notification.t('data)))
      = "getNotifications";
  };

  module ShowNotification
  {
    [@bs.send]
    external
    withOptions: (t, string, Notifications.NotificationOptions.t('data))
      => Js.Promise.t(unit)
      = "showNotification";

    [@bs.send] external withoutOptions: (t, string)
      => Js.Promise.t(unit)
      = "showNotification";
  };
};

/* Properties */

// TODO is this type right?
[@bs.get] external scope: t => string = "scope";

[@bs.get]
external installing: t => Js.Nullable.t(ServiceWorker.t) = "installing";

[@bs.get]
external waiting: t => Js.Nullable.t(ServiceWorker.t) = "waiting";

[@bs.get] external active: t => Js.Nullable.t(ServiceWorker.t) = "active";

// TODO navigationPreload see README.

[@bs.get] external pushManager: t => Push.PushManager.t = "pushManager";

// TODO sync property -- not standardized yet 2020-08-15

// TODO periodicSync -- not standardized yet and hardly implemented 2020-08-15

/* Event Handlers */

type handler = unit => unit;

[@bs.get]
external get_onupdatefound: t => Js.Nullable.t(handler) = "onupdatefound";

[@bs.set] external set_onupdatefound: (t, handler) => unit = "onupdatefound";

/* Methods */

let getNotifications = (~options=?, t):
  Js.Promise.t(array(Notifications.Notification.t('data))) =>
{
  switch(options) {
  | None => Private.GetNotifications.withoutOptions(t);
  | Some(o) => Private.GetNotifications.withOptions(t, o);
  };
};

let showNotification = (t, ~options=?, ~title: string): Js.Promise.t(unit) =>
{
  switch(options) {
  | None => Private.ShowNotification.withoutOptions(t, title);
  | Some(o) => Private.ShowNotification.withOptions(t, title, o);
  };
};

[@bs.send] external update: t => Js.Promise.t(t) = "update";

[@bs.send] external unregister: t => Js.Promise.t(bool) = "unregister";
