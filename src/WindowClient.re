type windowClient;
type t = windowClient;
module Impl = (T: { type t; }) =>
{
  type visibilityState = [
    | `hidden
    | `visibile
    | `prerender
  ];

  [@bs.get] external focused: windowClient => bool = "focused";
  [@bs.get] external visibilityState: windowClient
    => visibilityState
    = "visibilityState";

  [@bs.send] external focus: windowClient
    => Js.Promise.t(windowClient)
    = "focus";

  [@bs.send] external navigate: (windowClient, string)
    => Js.Promise.t(windowClient)
    = "navigate";
}

include Client.Impl({ type nonrec t = t; });