type _worker('a);
type t_like('a) = AbstractWorker.t_like(_worker('a));
type t = t_like(Dom._baseClass);

module Private
{
  module Make
  {
    [@bs.new] external withoutOptions: string => t = "Worker";
    [@bs.new]
    external withOptions: (string, WorkerOptions.t) => t = "Worker";
  };

  module PostMessage
  {
    [@bs.send]
    external withTransferList: (t_like('subtype), 'message, 'transferList)
      => unit
      = "postMessage";
    [@bs.send]
    external withoutTransferList: (t_like('subtype), 'message)
      => unit
      = "postMessage";
  }; 
};

let make = (~options=?, ~aURL: string): t =>
{
  switch (options) {
  | None => Private.Make.withoutOptions(aURL)
  | Some(o) => Private.Make.withOptions(aURL, o)
  };
};

/* Properties */

type messageHandler = MessageEvent.t => unit;

[@bs.get]
  external get_onmessage: t_like('subtype) => Js.Nullable.t(messageHandler)
  = "onmessage";

[@bs.set]
external set_onmessage: (t_like('subtype), messageHandler) => unit
  = "onmessage";

/* TODO is there a name for this type already? */
type errorHandler = unit => unit;

[@bs.get]
external get_onmessageerror: t_like('subtype) => Js.Nullable.t(errorHandler)
  = "onmessageerror";

[@bs.set]
external set_onmessageerror: (t_like('subtype), errorHandler) => unit
  = "onmessageerror";

[@bs.get]
external get_onrejectionhandled: PromiseRejectionEvent.t('a, 'reason)
  => PromiseRejectionEvent.handler('a, 'reason)
  = "onrejectionhandled";

[@bs.set]
external
set_onrejectionhandled:
  (PromiseRejectionEvent.t('a, 'reason),
  PromiseRejectionEvent.handler('a, 'reason))
  => unit
  = "onrejectionhandled";

[@bs.get]
external
get_onunhandledrejection: PromiseRejectionEvent.t('a, 'reason)
  => PromiseRejectionEvent.handler('a, 'reason)
  = "onunhandledrejection";

[@bs.set]
external
  set_onunhandledrejection:
    (PromiseRejectionEvent.t('a, 'reason),
    PromiseRejectionEvent.handler('a, 'reason))
    => unit
    = "onunhandledrejection";

/* Methods */

let postMessage = (~t: t_like('subtype),
    ~transferList=?,
    ~message: 'message): unit
=>
{
  switch (transferList) {
  | None => Private.PostMessage.withoutTransferList(t, message);
  | Some(o) => Private.PostMessage.withTransferList(t, message, o);
  };
};

[@bs.send] external terminate: t_like('subtype) => unit = "terminate";

